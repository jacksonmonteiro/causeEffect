//Take the list elements and turn them into a Node List
const node = document.querySelectorAll('.name');

//turns node into an array
const names = Array.from(node);

//Object with the information of the users of the list
const people = [
		{name: 'Peter Jenkins', street: '55 Winchester Street', city: 'Los Angeles', state: 'CA', country: 'USA', telephone: '213-555-0127', birthday: '04/20/1998'},
		{name: 'Gwen White', street: '39 Wayne Street', city: 'Orlando', state: 'FL', country: 'USA', telephone: '407-555-0180', birthday: '09/07/2002'},
		{name: 'Cordelia Price', street: '899 Cross Street', city: 'Austin', state: 'TX', country: 'USA', telephone: '512-555-0172', birthday: '01/15/2000'},
		{name: 'Edmund Cook', street: '93 King Road', city: 'Oklahoma City', state: 'OK', country: 'USA', telephone: '405-555-0149', birthday: '12/19/1996'},
		{name: 'Helena Sander', street: '9088 Rock Maple Street', city: 'New York', state: 'NY', country: 'USA', telephone: '212-555-0155', birthday: '08/22/1992'},
		{name: 'Sabrina Armstrong', street: '24 Arlington Dr.', city: 'San Diego', state: 'CA', country: 'USA', telephone: '619-555-0109', birthday: '06/01/2001'},
		{name: 'Serena Jackson', street: '935 American Lane', city: 'Hollywood', state: 'FL', country: 'USA', telephone: '323-555-0152', birthday: '02/14/2002'}
	];

//Values that will be changed in HTML
const name = document.querySelector('#name');
const address = document.querySelector('#address');
const phone = document.querySelector('#phone');
const birthday = document.querySelector('#birthday');

//event listener and value change on click
names[0].onclick = () => {
	name.innerHTML = `Name: <strong>${people[0].name} </strong>`;
	address.innerHTML = `Address: <strong>${people[0].street}, ${people[0].city}, ${people[0].state}, ${people[0].country}</strong>`;
	phone.innerHTML = `Phone: <strong>${people[0].telephone}</strong>`;
	birthday.innerHTML = `Birth date: <strong> ${people[0].birthday} </strong>`;
}

names[1].onclick = () => {
	name.innerHTML = `Name: <strong>${people[1].name} </strong>`;
	address.innerHTML = `Address: <strong>${people[1].street}, ${people[1].city}, ${people[1].state}, ${people[1].country}</strong>`;
	phone.innerHTML = `Phone: <strong>${people[1].telephone}</strong>`;
	birthday.innerHTML = `Birth date: <strong> ${people[1].birthday} </strong>`;
}

names[2].onclick = () => {
	name.innerHTML = `Name: <strong>${people[2].name} </strong>`;
	address.innerHTML = `Address: <strong>${people[2].street}, ${people[2].city}, ${people[2].state}, ${people[2].country}</strong>`;
	phone.innerHTML = `Phone: <strong>${people[2].telephone}</strong>`;
	birthday.innerHTML = `Birth date: <strong> ${people[2].birthday} </strong>`;
}

names[3].onclick = () => {
	name.innerHTML = `Name: <strong>${people[3].name} </strong>`;
	address.innerHTML = `Address: <strong>${people[3].street}, ${people[3].city}, ${people[3].state}, ${people[3].country}</strong>`;
	phone.innerHTML = `Phone: <strong>${people[3].telephone}</strong>`;
	birthday.innerHTML = `Birth date: <strong> ${people[3].birthday} </strong>`;
}

names[4].onclick = () => {
	name.innerHTML = `Name: <strong>${people[4].name} </strong>`;
	address.innerHTML = `Address: <strong>${people[4].street}, ${people[4].city}, ${people[4].state}, ${people[4].country}</strong>`;
	phone.innerHTML = `Phone: <strong>${people[4].telephone}</strong>`;
	birthday.innerHTML = `Birth date: <strong> ${people[4].birthday} </strong>`;
}

names[5].onclick = () => {
	name.innerHTML = `Name: <strong>${people[5].name} </strong>`;
	address.innerHTML = `Address: <strong>${people[5].street}, ${people[5].city}, ${people[5].state}, ${people[5].country}</strong>`;
	phone.innerHTML = `Phone: <strong>${people[5].telephone}</strong>`;
	birthday.innerHTML = `Birth date: <strong> ${people[5].birthday} </strong>`;
}

names[6].onclick = () => {
	name.innerHTML = `Name: <strong>${people[6].name} </strong>`;
	address.innerHTML = `Address: <strong>${people[6].street}, ${people[6].city}, ${people[6].state}, ${people[6].country}</strong>`;
	phone.innerHTML = `Phone: <strong>${people[6].telephone}</strong>`;
	birthday.innerHTML = `Birth date: <strong> ${people[6].birthday} </strong>`;
}